# Desktop Timer

http://timer.housser.is

## Building Web App

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## Building Desktop App

### Requirements

- Rust / Cargo

```bash
cargo tauri build
```
