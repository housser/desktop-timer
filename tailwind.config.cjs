const colors = require('tailwindcss/colors');

const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		container: {
			center: true
		},
		extend: {
			colors: {
				primary: colors.orange[400]
			}
		}
	},

	plugins: []
};

module.exports = config;
