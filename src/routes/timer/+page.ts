/** @type {import('./$types').PageLoad} */
export function load({ url }) {
  return {
    hours: parseInt(url.searchParams.get('hours'), 10) || 0,
    minutes: parseInt(url.searchParams.get('minutes'), 10) || 0,
    seconds: parseInt(url.searchParams.get('seconds'), 10) || 0,
  }
}